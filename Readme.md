# Analytics in python training

Trainer: Dr. Radoslav Pauco

1. pre-training exercise on basics of pandas and matplotlib (WEEK 0)
2. setting up local environment or working in the cloud (WEEK 0)
3. basic statistics tools, exploratory data analysis (WEEK 1)
4. basic linear regression (WEEK 2)
5. multi-class multi-label classification (WEEK 2, 3)
6. anomalies & clustering (WEEK 3)

- WEEK 0 - without meeting 
- WEEK 1, 2, 3 - online meetings
- we are starting with 0 because we're python positive
